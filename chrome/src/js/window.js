$(document).ready(function(){
	var bg = chrome.extension.getBackgroundPage();

	// If first time user uses extension: Open settings
	if (bg.settings == null)
		openSettings ();
	else 
		initLayout ();

	$("#settings a").click (function () { openSettings () });
});

// Initialise the layout
function initLayout () {
	var bg = chrome.extension.getBackgroundPage();

	// Remove content and old click listeners
	$("#content").html("");
	$("#content").off("click");
	
	// Iterate through repositories
	$.each(bg.repos, function(index, repo) {
		var commits = "";
		var url = "https://bitbucket.org/" +  repo.user + "/" + repo.name;

		// Add commit messages
		$.each(repo.commits, function(index, commit) {
			commits += 
				"<div class='commit' id='" + commit.hash + "'>\
					<div class='message'>\
						<a href='" + url + "/commits/" + commit.hash + "' target='_blank'>" + $("<div/>").text(commit.message).html() + "</a>\
					</div>\
					<div class='mark'><a href='#'><img src='img/mark.png' /></a></div>\
				</div>";

			var id = "#" + commit.hash;
			$("#content").on('click', id + " .message a", function(event) { commitClicked (event, commit, repo); });
			$("#content").on('click', id + " .mark a", function(event) { markCommitClicked (event, commit, repo); });
		});
		
		// Not all commits can be displayed
		if (repo.commits.length < repo.unread)
			commits += "<div class='commit center'><a href='" + url + "' target='_blank'>...</a></div>";
  		
  		// Add repository header
  		var content = " \
  			<div class='repository' id='rep_" + repo.name + "'> \
  				<div class='title'> \
  					<div class='name'>\
  						<a href='" + url + "' target='_blank'>" + repo.display_name + "</a>\
  					</div>";

		if (commits.length > 0)
			content += "<div class='mark'><a href=''><img src='img/markAll.png' /></a></div>";

		content += "</div><div class='commits'>" + commits + "</div></div>";

  		$("#content").append(content);

  		// Add click listeners
  		var id = "#rep_" + repo.name;
  		$("#content").on('click', id + " .name a", function(event) { repoClicked (event, repo); });
  		$("#content").on('click', id + " .title .mark a", function(event) { markRepoClicked (event, repo); });
	});
}

// Commit is clicked: Remove it
function commitClicked (event, commit, repo) {
	var bg = chrome.extension.getBackgroundPage();
	bg.removeCommit (commit, repo);
}

// Commit eye is clicked: Remove and update layout
function markCommitClicked (event, commit, repo) {
	var bg = chrome.extension.getBackgroundPage();

	event.preventDefault ();
	$("#" + commit.hash).remove();
	bg.removeCommit (commit, repo);

	initLayout ();
}

// Repository title is clicked: Remove all commits
function repoClicked (event, repo) {
	var bg = chrome.extension.getBackgroundPage();
	bg.removeAllCommits (repo);
}

// Repository title eye is clicked: Remove all commits and update layout
function markRepoClicked (event, repo) {
	var bg = chrome.extension.getBackgroundPage();

	event.preventDefault ();
	$("#rep_" + repo.name + " .commits").remove();
	bg.removeAllCommits (repo);

	initLayout ();
}

// Open the settings popup and close the default_popup
function openSettings () {
	chrome.windows.create({ 'url': 'data.html', 'type': 'popup', 'width' : 510, 'height' : 600 }, function(window) {});
	window.close();
}