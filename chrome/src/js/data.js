var phase;
var version = 1.54;
var old_repos = [];
var already_displayed = [];

var user;
var pass;
var repos = [];

$(document).ready(function(){
	phase = 1;

	loadData ();
	updateLayout ();
	initListeners ();
});

// Load data from background page
function loadData () {
	var bg = chrome.extension.getBackgroundPage();

	if (bg.settings != null) {
		$("#user-input").val (bg.settings.user);
		$("#pass-input").val (bg.settings.password);
		checkInput ();
	}

	if (bg.repos != null) {
		$.each (bg.repos, function (index, repo) {
			old_repos.push (repo.user + "/" + repo.name);
		});
	}
}

// Hide all phases except current
function updateLayout () {
	// Only show current phase
	$(".phase").hide ();
	$("#phase-" + phase).show ();

	// Update next en previous button
	checkInput ();
	if (phase > 1)
		$(".prev").removeAttr ("disabled", "disabled");
	else
		$(".prev").attr("disabled", "disabled");
}

function initListeners () {
	$("#user-input").keyup (function() { checkInput (); });
	$("#pass-input").keyup (function() { checkInput (); });
	$(".next").click (function() { nextClicked (); });
	$(".prev").click (function() { prevClicked (); });
	$("#fetchRepoDiffOwner").click (function() {fetchReposDiffOwner (); });
}

// Check for changes in forms
function checkInput () {
	if (phase == 1) {
		if ($("#user-input").val().length > 0 && $("#pass-input").val().length > 0)
			$(".next").removeAttr("disabled");
		else
			$(".next").attr("disabled","disabled");
	}
}

// Goto next phase
function nextClicked () {
	// Hide old errors
	$(".error").hide ()

	switch (phase) {
		case 1: 
			gotoPhase2Clicked ();
			break;
		case 2:
			gotoPhase3 ();
			break;
		case 3:
			finish ();
			break;
	}
}

// Go to previous phase
function prevClicked () {
	phase--;
	updateLayout ();
}

// Show an error message
function showError (message) {
	$(".error").show ();
	$(".error").html (message);
}

// Check if user is already following repo (before he opened the settings)
function isInOldRepos (full_name) {
	var output = false;

	$.each (old_repos, function(index, repoName) {
		if (repoName === full_name) {
			output = true;
			return false;
		}
	});

	return output;
}

// Check if repo is selected
function isInRepos (name) {
	var output = false;

	$.each (repos, function(index, repo) {
		if (repo.name === name) {
			output = true;
			return false;
		}
	});

	return output;
}

// A username or url is submitted: Find repositories
function fetchReposDiffOwner () {
	var extraUrl = "";

	// Remove old handlers
	$("#results-search").off ();

	// Get username or repo name
	var input = $("#diffUser").val().split("/");
	var diff_user = null;
	var diff_proj = null;

	if (input.length == 1) {
		if (input[0].trim().length == 0) {
			$("#results-search").html ("No results.");
			return;
		}
		diff_user = input[0];

	} else if (input.length > 1) {
		diff_user = input[input.length-2];
		diff_proj = input[input.length-1];
		extraUrl = "/" + diff_proj;

	}

	// Loading
	$("#results-search").html ("Loading...");

	// Download data
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://bitbucket.org/api/2.0/repositories/' + diff_user + extraUrl);
	xhr.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass));
	xhr.responseType = 'json';
	
	xhr.onload = function(e) {
		$("#results-search").html ("");

		// User specified
		if (input.length == 1) {
			if (e.srcElement.status != 200 || e.srcElement.response.values.length == 0)
				$("#results-search").html ("No results.");

			else {
				$.each(e.srcElement.response.values, function(index, value) {
					if (!isAlreadyDisplayed (value.full_name)) {
						var id = value.full_name.split("/");
						id = id[0] + "-" + id[1];

						var content = "<div><label><input type='checkbox' id='" + id + "' value='" + value.full_name + "'><span>" + value.name +"</span></label></div>";
						$("#results-search").append (content);
						$("#results-search").on('click', "#"+id, function(event) { addRepoDiffUser ($(this), value); });
					}
				});
			}

			if ($("#results-search").html().length == 0) {
				$("#results-search").html ("No results.");
			}
		
		// Repository specified
		} else {
			if (e.srcElement.status != 200)
				$("#results-search").html ("No results.");
			
			else if (!isAlreadyDisplayed (e.srcElement.response.full_name)) {
				var value = e.srcElement.response;
				var id = value.full_name.split("/");
				id = id[0] + "-" + id[1];
				
				var content = "<div><label><input type='checkbox' id='" + id + "' value='" + value.full_name + "'><span>" + value.name +"</span></label></div>";
				$("#results-search").html (content);
				$("#results-search").on('click', "#"+id, function(event) { addRepoDiffUser ($(this), value); });
			
			} else {
				$("#results-search").html ("No results.");
			}
		}
	};

	// Connection error
	xhr.onerror = function(e) {
		showError("Network connection problems.");
	}
	
	xhr.send();
}

// User checked a repository owned by a different user
function addRepoDiffUser (target, commit) {
	// Remove click handler
	$("#results-search").off ("click", "#"+target[0].id);

	// Hide
	target.parent().parent().hide ("slow", function () {
		target.remove();
		var add = "<label><input type='checkbox' value='" + commit.full_name + "' checked='checked'><span>" + commit.name +"</span></label><br />";
		$(add).hide().appendTo ("#repoListDiffUser .content").show ("slow");
	});

	// Add to list
	$("#repoListDiffUser").show ("slow");

	// Update already_displayed list
	already_displayed.push (commit.full_name);

}

// Is this repository already displayed somewhere?
function isAlreadyDisplayed (fullName) {
	var output = false;

	$.each(already_displayed, function(index, value) {
		if (value === fullName) {
			output = true;
			return false;
		}
	});

	return output;
}

// Goto next phase if login is correct
function gotoPhase2Clicked () {
	// Get data
	user = $("#user-input").val();
	pass = $("#pass-input").val();

	// Show status
	$(".next").val ("Logging in..");
	$(".next").attr("disabled","disabled");

	// Check login
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://bitbucket.org/api/1.0/user/');
	xhr.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass));
	xhr.responseType = 'json';
	
	// Got a response
	xhr.onload = function(e) {
		$(".next").val ("Next");
		$(".next").removeAttr ("disabled");

		if (e.srcElement.status == 200)
			gotoPhase2();
		else
			showError ("Wrong username or password.");
	};

	// Connection error
	xhr.onerror = function(e) {
		$(".next").val ("Next");
		$(".next").removeAttr ("disabled");
		showError("Network connection problems.");
	}

	xhr.send();
}

// Goto phase 2, and load all repositories
function gotoPhase2 () {
	// Show next layout
	phase = 2;
	updateLayout ();

	// Fetch data
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://bitbucket.org/api/2.0/repositories/' + user);
	xhr.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass));
	xhr.responseType = 'json';
	
	xhr.onload = function(e) {
		// Only add them to list once
		var addToAlreadyDisplayed = already_displayed.length == 0;
		
		// Add a list of repositories owned by user
		var content = "";
		$.each(e.srcElement.response.values, function(index, value) {
			var checked = "";
			if (isInOldRepos (value.full_name))
				checked = " checked='checked'";

			if (addToAlreadyDisplayed)
				already_displayed.push (value.full_name);

			content += "<label><input type='checkbox' value='" + value.full_name + "'" + checked + "><span>" + value.name +"</span></label><br />";
		});

		if (content.length == 0) {
			$("#all-repos").html ("<span class='info'>There are no repositories.</span>");
			showError (user + " has no repositories. If this is incorrect make sure you've entered the correct password.");
		} else {
			$("#all-repos").html (content);
		}

		// Add repos that user selected but aren't displayed yet
		$.each(old_repos, function (index, repo) {
			if (!isAlreadyDisplayed (repo)) {
				// Add to list
				$("#repoListDiffUser").show ();
				$("#repoListDiffUser .content").append (
					"<label><input type='checkbox' value='" + repo + "' checked='checked'><span>" + getDisplayName (repo) +"</span></label><br />"
				);

				already_displayed.push (repo);
			}
		});
	};

	// Connection error
	xhr.onerror = function(e) {
		showError("Network connection problems.");
	}
	
	xhr.send();
}

// Goto phase 3: Show all selected repositories
function gotoPhase3 () {
	// Update layout
	phase = 3;
	updateLayout ();
	$("#phase-3 .next").val ("Finish");

	// Show selected repositories
	repos = [];
	var content = addReposFromLabels ($("#all-repos").children ("label"));
	content += addReposFromLabels ($("#repoListDiffUser .content").children ("label"));

	if (content.length == 0) {
		$("#all-repos-final").html ("<span class='info'>No repositories selected.</span>");
	} else
		$("#all-repos-final").html (content);
}

// Add repos that are selected (by reading the label containing the checkbox)
function addReposFromLabels (labels, content) {
	var content = "";

	$.each(labels, function (index, label) {
		input = label.children[0];
		
		if (input.checked) {
			var split = input.value.split("/");
			content += input.value + "<br />";
			repos.push({
				"user" : split[0],
				"name" : split[1],
				"display_name" : label.getElementsByTagName("span")[0].innerHTML
			});
		}
	});

	return content;
}

// Save data and close window
function finish () {
	$("#phase-3 .next").val ("Saving..");

	var settings = {
		"newShownLimit" : 5,
		"user" : user,
		"password" : btoa (pass),
		"loadLimit" : 30
	};

	var repositories = [];
	var bg = chrome.extension.getBackgroundPage();

	// Keep selected old repositories, and remove other old repositories
	if (bg.repos != null) {
		repositories = bg.repos;
		remove = [];

		// Detect which repos should be removed
		$.each(repositories, function (index, repo) {
			if (!isInRepos (repo.name))
				remove.push (index);
		});

		// Remove detected repos
		var offset = 0;
		for (var i=0; i<remove.length; i++) {
			repositories.splice (remove[i] - offset, 1);
			offset++;
		}
	}
	
	// Add new repositories
	$.each(repos, function(index, repo) {
		if (!isInOldRepos (repo.user + "/" + repo.name)) {
			repositories.push ({
				"user" : repo.user, "name" : repo.name, "display_name" : repo.display_name, "last_update" : 0,
				"unread" : 0, "commits" : [], "ignore" : [], "e" : null, "last_notification" : 0
			});
		}
	});

	chrome.storage.local.set({"version" : version, "settings": settings, "repos" : repositories}, function() {
		var bg = chrome.extension.getBackgroundPage();
		bg.loadData ();
        window.close ();
    });
}

// Get display name of a repo
function getDisplayName (full_name) {
	var bg = chrome.extension.getBackgroundPage();
	var _repos = bg.repos;
	var output = full_name;

	$.each (_repos, function (index, repo) {
		if (repo.user + "/" + repo.name === full_name) {
			output = repo.display_name;
			return false;
		}
	});

	return output;
}